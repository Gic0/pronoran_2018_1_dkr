/*
 *
 * Константы
 * */

var SLIDES_LOADING_COMPLETE = "slideLoadingComplete";
var SLIDES_LOADING_START = "slideLoadingStart";
var SLIDE_ANIMATION_COMPLETE = "onAnimationComplete";
var SLIDE_ANIMATION_START = "onAnimationStart";
var SLIDE_GOTO = "onGoToStart";

/*
 *
 * Сценарий
 *
 * */

function Slide(slide_data, scenario, slides) {
    this.folder = null;
    this.scenario = scenario;

    if (typeof slide_data.prev_slide == "undefined" && slide_data.prev_slide != -1) {
        var prev_slide = slides.filter(function (slide) {
            return slide.next_slide === slide_data.id;
        });
        try {
            slide_data.prev_slide = prev_slide[0].id;
        } catch (e) {
            slide_data.prev_slide = -1;
        }
    }

    for (var param in slide_data) {
        this[param] = slide_data[param]
    }

    this.is_first = typeof this.is_first == "undefined" ? false : this.is_first;
    if (this.is_first) {
        this.scenario.first_slide_id = this.id;
    }

    //Свойства, для сгенерированного сценария, хранящие номер временного следующего и временного предыдущего слайда.
    //После того, как сценарий будет завершен, свойства становятся равными null.
    return this;
};


Slide.prototype = {
    //Возвращает следующий слайд
    getNextSlide: function () {
        ////console.log('get next_slide');
        //Если это сгенерированный сценрий, возвращаем номер временного следующего слайда

        return this.scenario.getSlide(this.next_slide);
    },
    //Возвращает предыдущий слайд
    getPrevSlide: function () {
        ////console.log('get prev_slide');
        //Если это сгенерированный сценрий, возвращаем временный номер предыдущего слайда

        return this.scenario.getSlide(this.prev_slide);
    },

    //Изменение у текущего слайда значение следующего слайда
    setNextSlide: function (next_slide) {
        ////console.log('setNextSlide');
        this.next_slide = next_slide.id;
    },
    //Изменение у текущего слайда значение предыдущего слайда
    setPrevSlide: function (prev_slide) {
        ////console.log('setPrevSlide');
        this.prev_slide = prev_slide.id;
    },
    //Подгрузка html, и css/js файлов слайда
    requestSlideContent: function () {
        var slide = this;
        var deffered = $.Deferred();
        $.get('' + slide.folder + '/slide.html', function (data) {
            deffered.resolve(data);
        });

        return deffered.promise();
    }
}

function ScenarioController() {
    this.scenario = {};
    this.first_slide_id = 1;
    this.current_slide_id = 1;
};

ScenarioController.prototype = {
    //Преобразует json сценарий в набор обьектов, класса Slide
    importScenario: function (slides) {
        //console.log('parseSlides');
        var scenario_controller = this;
        $.each(slides, function (index, slide) {
            var slide = new Slide(slide, scenario_controller, slides);
            scenario_controller.scenario[slide.id] = slide;
        });
    },
    toArray: function () {
        //console.log('toArray');
        var scenario_array = [];
        for (var index in this.scenario) {
            var slide = this.scenario[index]
            scenario_array.push(slide)
        }
        return scenario_array;
    },
    //Возвращает слайд по его id
    getSlide: function (slide_id) {
        //console.log('getSlide');
        try {
            return this.scenario[slide_id];
        } catch (e) {
            //console.error('slide by id ' + slide_id + 'not found');
        }
    },
    //Возвращает первый слайд
    getFirstSlide: function () {
        return this.getSlide(this.first_slide_id);
    },
    setCurrentSlide: function (slide) {
        this.current_slide_id = slide.id;
    },
    getCurrentSlide: function () {
        return this.getSlide(this.current_slide_id);
    }
}

function PresentationCore(device, animations) {
    this.device = device;
    this.animations = animations;

    //Главный контенер
    this.base_container = $('#base_container');
    //Контейнер для подгрузки возможных следующих слайдов
    //Контенер в котором находятся слайды
    this.slide_container = $('#slide_container');

    //Контенеры с предыдущим, текущим и следующим слайдом
    this.prev_container = $('.slide:nth-child(1)');
    this.curr_container = $('.slide:nth-child(2)');
    this.next_container = $('.slide:nth-child(3)');

    //Размеры контенера с слайдами
    this.slide_container.css('width', this.device.width + 'px');
    //Размеры главного контенера
    this.base_container.css('width', this.device.width + 'px');

    //Инициализируем предыдущий, текущий и следующий контенеры
    init_container(this.prev_container, this.device);
    init_container(this.curr_container, this.device);
    init_container(this.next_container, this.device);

    //Инициализация контейнера
    function init_container(container, device) {
        container.css('width', device.width + 'px');
        container.css('opacity', 0);
        container.css('display', 'none');
    }
};

PresentationCore.prototype = {
    //Очистить контейнер слайда
    clearContainer: function (container) {
        container.html('');
    },
    //Поместь в контенер информацию из слайда, контейнеру слайда задается класс по названию папки со слайдом
    fillContainer: function (slide, container, content, promise) {
        this.clearContainer(container);
        container.html(content);
        container.addClass(slide.folder);
        if (typeof promise != "undefined") {
            promise.resolve();
        }
    },
    goToSlide: function (current_slide) {
        this.prev_container.remove();
        this.slide_container.prepend('<div class="slide" style="display:none;width:' + this.device.width + 'px;opacity:0;"></div>');
        this.prev_container = $('.slide:nth-child(1)');

        this.curr_container.remove();
        this.curr_container = this.next_container;
        this.slide_container.append('<div class="slide" style="display:none;width:' + this.device.width + 'px;opacity:0;"></div>');
        this.next_container = $('.slide:nth-child(3)');

        var prev_container_promise = $.Deferred();
        var next_container_promise = $.Deferred();
        var promises = [];
        var prev_slide = current_slide.getPrevSlide();
        var next_slide = current_slide.getNextSlide();

        try {
            console.log(prev_slide.id);
            console.log(current_slide.id);
            console.log(next_slide.id);
        } catch (e) {

        }

        this.tryLoadSlideContent(prev_slide, presentationCore.prev_container, prev_container_promise);
        this.tryLoadSlideContent(next_slide, presentationCore.next_container, next_container_promise);

        scenarioController.setCurrentSlide(current_slide);

        promises.push(prev_container_promise.promise());
        promises.push(next_container_promise.promise());

        //Как только все загружено, показываем первый слайд
        $.when.apply($, promises).done(function () {
            $(document).trigger(SLIDES_LOADING_COMPLETE);
        });
    },
    goToNext: function () {
        this.prev_container.remove();
        this.slide_container.prepend('<div class="slide" style="display:none;width:' + this.device.width + 'px;opacity:0;"></div>');
        this.prev_container = $('.slide:nth-child(1)');

        this.curr_container.remove();
        this.curr_container = this.next_container;
        this.slide_container.append('<div class="slide" style="display:none;width:' + this.device.width + 'px;opacity:0;"></div>');
        this.next_container = $('.slide:nth-child(3)');


        var prev_container_promise = $.Deferred();
        var next_container_promise = $.Deferred();
        var promises = [];
        var prev_slide = scenarioController.getCurrentSlide();
        var next_slide = prev_slide.getNextSlide().getNextSlide();
        var current_slide = prev_slide.getNextSlide();

        try {
            console.log(prev_slide.id);
            console.log(current_slide.id);
            console.log(next_slide.id);
        } catch (e) {

        }

        this.tryLoadSlideContent(prev_slide, presentationCore.prev_container, prev_container_promise);
        this.tryLoadSlideContent(next_slide, presentationCore.next_container, next_container_promise);

        scenarioController.setCurrentSlide(current_slide);

        promises.push(prev_container_promise.promise());
        promises.push(next_container_promise.promise());

        //Как только все загружено, показываем первый слайд
        $.when.apply($, promises).done(function () {
            $(document).trigger(SLIDES_LOADING_COMPLETE);
        });
    },
    goToPrev: function () {
        this.curr_container.remove();
        this.curr_container = this.prev_container;

        this.next_container.remove();

        this.slide_container.prepend('<div class="slide" style="display:none;width:' + this.device.width + 'px;opacity:0;"></div>');
        this.prev_container = $('.slide:nth-child(1)');
        this.slide_container.append('<div class="slide" style="display:none;width:' + this.device.width + 'px;opacity:0;"></div>');
        this.next_container = $('.slide:nth-child(3)');

        var prev_container_promise = $.Deferred();
        var next_container_promise = $.Deferred();

        //todo
        var promises = [];

        var current_slide = scenarioController.getCurrentSlide().getPrevSlide();

        var prev_slide = current_slide.getPrevSlide();
        var next_slide = current_slide.getNextSlide();

        try {
            console.log(prev_slide.id);
            console.log(current_slide.id);
            console.log(next_slide.id);
        } catch (e) {

        }

        this.tryLoadSlideContent(prev_slide, presentationCore.prev_container, prev_container_promise);
        this.tryLoadSlideContent(next_slide, presentationCore.next_container, next_container_promise);

        scenarioController.setCurrentSlide(current_slide);

        promises.push(prev_container_promise.promise());
        promises.push(next_container_promise.promise());

        //Как только все загружено, показываем первый слайд
        $.when.apply($, promises).done(function () {
            $(document).trigger(SLIDES_LOADING_COMPLETE);
        });
    },
    tryLoadSlideContent: function (slide, container, promise) {
        try {
            var request = slide.requestSlideContent();
            $.when(request).done(function (content) {
                presentationCore.fillContainer(slide, container, content, promise);
            });
        } catch (e) {
            promise.resolve();
            //Слайда не существует
            //console.warn('Слайд не найден');
        }

    },
    notifyApp: function () {
        var iframe = document.createElement("IFRAME");
        document.documentElement.appendChild(iframe);
        iframe.parentNode.removeChild(iframe);
        iframe = null;
        console.log('notifyApp');
        console.log('scenarioController.getCurrentSlide');
        console.log(scenarioController.getCurrentSlide().id);
        AndroidAdapter.getCurrentSlide();
    }
}


var presentationCore;
var scenarioController;

function PresentationApi(device, animations) {
    presentationCore = new PresentationCore(device, animations);
    scenarioController = new ScenarioController();
    return this;
}

PresentationApi.prototype = {
    goToSlide: function (next_slide, animation) {
        $(document).trigger(SLIDE_GOTO, next_slide);

        animation = typeof animation == "undefined" ? presentationCore.animations.animateToNextSlide : animation;

        presentationCore.next_container.remove();
        presentationCore.slide_container.append('<div class="slide" style="display:none;width:' + presentationCore.device.width + 'px;opacity:0;"></div>');
        presentationCore.next_container = $('.slide:nth-child(3)');

        var next_container_promise = $.Deferred();
        var promises = [];

        presentationCore.tryLoadSlideContent(next_slide, presentationCore.next_container, next_container_promise);
        promises.push(next_container_promise.promise());


        animation = typeof animation == "undefined" ? presentationCore.animations.animateToNextSlide : animation;

        var after = function () {
            $(document).trigger(SLIDE_ANIMATION_COMPLETE);
            $(document).trigger(SLIDES_LOADING_START);
            presentationCore.goToSlide(next_slide);
            presentationCore.notifyApp();
        }
        $.when.apply($, promises).done(function () {
            if (presentationCore.next_container.html().length > 0) {
                $(document).trigger(SLIDE_ANIMATION_START);
                animation(presentationCore.curr_container, presentationCore.next_container, after);
            }
        });
    },
    goToNext: function (animation) {
        animation = typeof animation == "undefined" ? presentationCore.animations.animateToNextSlide : animation;

        var after = function () {
            $(document).trigger(SLIDE_ANIMATION_COMPLETE);
            $(document).trigger(SLIDES_LOADING_START);
            presentationCore.goToNext();
            presentationCore.notifyApp();
        }

        if (presentationCore.next_container.html().length > 0) {
            $(document).trigger(SLIDE_ANIMATION_START);
            animation(presentationCore.curr_container, presentationCore.next_container, after);
        }

    },
    goToPrev: function (animation) {
        animation = typeof animation == "undefined" ? presentationCore.animations.animateToPrevSlide : animation;
        var after = function () {
            $(document).trigger(SLIDE_ANIMATION_COMPLETE);
            $(document).trigger(SLIDES_LOADING_START);
            presentationCore.goToPrev();
            presentationCore.notifyApp();
        }

        if (presentationCore.prev_container.html().length > 0) {
            $(document).trigger(SLIDE_ANIMATION_START);
            animation(presentationCore.curr_container, presentationCore.prev_container, after);
        }
    },
    getCurrentSlide: function () {
        return scenarioController.getCurrentSlide();
    },
    getSlide: function (slide_id) {
        return scenarioController.getSlide(slide_id);
    },
    start: function () {
        var promises = [];

        //console.log('starting...');
        //Подгрузка ajax`сом json сценария
        $.ajaxPrefilter("json script", function (options) {
            options.crossDomain = true;
        });

        $.ajax({
            url: "config/scenario.json",
            dataType: 'json',
            success: function (data) {
                //Преобразует json сценрий в набор обьектов, класса Slide
                scenarioController.importScenario(data.scenario);

                //Возвращает первый слайд
                var first_slide = scenarioController.getFirstSlide();
                scenarioController.setCurrentSlide(first_slide);
                //Подгружает css и js файлов первого и и следующего слайдов
                var first_request = first_slide.requestSlideContent();
                var next_slide = first_slide.getNextSlide();
                var next_request = next_slide.requestSlideContent();
                var first_container_promise = $.Deferred();
                var next_container_promise = $.Deferred();

                $.when(first_request).done(function (content) {
                    presentationCore.fillContainer(first_slide, presentationCore.curr_container, content, first_container_promise);
                });

                $.when(next_request).done(function (content) {
                    presentationCore.fillContainer(next_slide, presentationCore.next_container, content, next_container_promise);
                });

                promises.push(first_container_promise.promise());
                promises.push(next_container_promise.promise());

                contentStorage = {
                    heap: {},
                    addData: function (name, value) {
                        this.heap[name] = value;
                    },

                    getData: function (name) {
                        if (typeof this.heap[name] === "undefined") {
                            return null;
                        }
                        return this.heap[name];
                    },

                    makeJson: function () {
                        var h = this.heap;
                        return JSON.stringify(h);
                    }
                }

                AndroidAdapter = {
                    getCurrentSlide: function () {
                        var current_slide = scenarioController.getCurrentSlide();
                        try {
                            console.log('Android.onSlideChanged(' + current_slide.id + ')');
                            Android.onSlideChanged(current_slide.id);
                        } catch (e) {
                            //ios или браузер
                        }
                    },
                    goToSlide: function (slide_id) {
                        var slide = presentationApi.getSlide(slide_id)
                        presentationApi.goToSlide(slide)
                    },
                    goToNext: function () {
                        presentationApi.goToNext();
                    },
                    goToPrev: function () {
                        presentationApi.goToPrev();
                    },
                    getJson: function () {
                        var json = contentStorage.makeJson();
                        try {
                            Android.onStatisticCollected(json);
                        } catch (e) {
                            //ios или браузер
                        }
                    }
                }

                iOSAdapter = {
                    getCurrentSlide: function () {
                        var current_slide = scenarioController.getCurrentSlide();
                        try {
                            return current_slide.id;
                            //Android.onSlideChanged(current_slide.id);
                        } catch (e) {
                            //ios или браузер
                        }
                    },
                    goToSlide: function (slide_id) {
                        var slide = presentationApi.getSlide(slide_id)
                        presentationApi.goToSlide(slide)
                    },
                    goToNext: function () {
                        presentationApi.goToNext();
                    },
                    goToPrev: function () {
                        presentationApi.goToPrev();
                    },
                    getJson: function () {
                        try {
                            return contentStorage.makeJson();
                        } catch (e) {

                        }
                    }
                }
                //Как только все загружено, показываем первый слайд
                $.when.apply($, promises).done(function () {
                    $(document).trigger(SLIDES_LOADING_COMPLETE);
                    presentationCore.animations.animateFirstSlide(presentationCore.curr_container, function () {
                        presentationCore.notifyApp();
                    });
                });
            },
            error: function () {
                $('body').html('Ошибка при загрузке сценария презентации');
            }
        });
    }
}
var contentStorage, AndroidAdapter, iOSAdapter;

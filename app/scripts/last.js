//Обязательно в области глобальной видимости, названия не менять:
var presentationApi, default_device, default_animations, default_navigation;

$(document).ready(function() {
  default_device = {
    os: "iOS",
    width: 1024,
    height: 768
  }

  default_animations = {
    //Обязательный метод
    animateFirstSlide: function(slide, after) {
      slide.show();
      slide.transit({
        opacity: 1
      }, 200, 'linear', after());
      /*
       slide.animate({
       opacity: 1
       }, 0, 'linear', after());*/
    },
    //Обязательный метод
    animateToNextSlide: function(current_slide, next_slide, after) {
      var promises = [];
      var anim1 = $.Deferred();
      var anim2 = $.Deferred();

      current_slide.transit({
          opacity: 0
        }, 140, 'linear',
        function() {
          current_slide.hide();
          anim1.resolve();
        }
      );
      next_slide.show();
      next_slide.transit({
        opacity: 1
      }, 120, 'linear', function() {
        anim2.resolve();
      });

      promises.push(anim1.promise())
      promises.push(anim2.promise())

      $.when.apply($, promises).done(function() {
        after();
      });
    },
    //Обязательный метод
    animateToPrevSlide: function(current_slide, prev_slide, after) {
      var promises = [];
      var anim1 = $.Deferred();
      var anim2 = $.Deferred();

      current_slide.transit({
          opacity: 0
        }, 300, 'linear',
        function() {
          current_slide.hide();
          anim1.resolve();
        }
      );

      prev_slide.show();
      prev_slide.transit({
        opacity: 1
      }, 200, 'linear', function() {
        anim2.resolve();
      });

      promises.push(anim1.promise())
      promises.push(anim2.promise())

      $.when.apply($, promises).done(function() {
        after();
      });
    }
  }

  default_navigation = {
    nav: function(event, gesture) {
      if (gesture.direction.lastX < 0) {
        console.log('to nxt');
        presentationApi.goToNext();

      } else {
        console.log('to prv');
        presentationApi.goToPrev();
      }
    }
  }

  //Инициализация презентации. Название объекта не изменять.
  presentationApi = new PresentationApi(default_device, default_animations);
  presentationApi.start();

  var base_container = $('#base_container');
  $(document).on(SLIDE_ANIMATION_START, function() {
    console.log('SLIDE_ANIMATION_START');
    base_container.swipe("destroy");
  });

  $(document).on(SLIDES_LOADING_COMPLETE, function() {
    console.log('SLIDES_LOADING_COMPLETE');
    base_container.swipe({
      swipeLeft: function(event, direction, distance, duration, fingerCount) {
        presentationApi.goToNext();
      },
      swipeRight: function(event, direction, distance, duration, fingerCount) {
        presentationApi.goToPrev();
      }
    });
  });
});

const slide = document.querySelector('.slide')
const slider = slide.querySelector('.slider')
const slideAnimator = new Animator(slide)
const sliderToggleShowBtn = slide.querySelector('.slider-btn')
slideAnimator.set(0) 
slider.style.height = '0px';
slider.style.margin = '0 auto 30px';

noUiSlider.create(slider, {
  start: [0],
  snap: true,
	range: {
    'min': 0,
		'16%': 1,
		'43%': 2,
		'72%': 3, 
		'max': 5 
	}
});


sliderToggleShowBtn.addEventListener('click', ()=>{
  slideAnimator.set(+!slideAnimator.get())
})


slider.noUiSlider.on('slide', function(){
  swipeBlock = true;
	console.log('slide')
});

slider.noUiSlider.on('set', function(){
  swipeBlock = false;
	console.log('set')
});

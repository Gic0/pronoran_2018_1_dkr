const slide = document.querySelector('.footer__ref')
const slideAnimator = new Animator(slide)
slideAnimator.set(0)

slide.addEventListener('click' , e=> {
console.log(!slideAnimator.get())
  if (!slideAnimator.get()) {
    slideAnimator.set(1)
} else {
    slideAnimator.set(0)
  }
})
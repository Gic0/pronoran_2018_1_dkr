const vizits = {
  "bp": ["bp_02","bp_03", "bp_04", "bp_05", "bp_06", "bp_07", "bp_08"],
  "ukr": ["ukr_02", "ukr_03", "ukr_04", "ukr_05_1", "ukr_06", "bp_08"]
};

const vizitNames = {
  "bp": 'Болезнь_Паркинсона',
  "ukr": 'УКР'
};

module.exports = {
  vizits,
  vizitNames
}
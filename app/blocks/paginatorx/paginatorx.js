class Paginator {
  constructor(el, maxPages){
    this._maxPages = maxPages;
    this._wrapper  = el;
    this._stateIndex = 1;
    this._init()
    
  }
  _init () {
    console.log('Paginator._init')
    let dots = '';
    for (var i = 1; i <= this._maxPages; i++) {
      if(i != this._stateIndex) { 
        dots += "<div class='dot'> </div>"
        }
      else{
        dots += "<div class='dot dot_active'> </div>"
        }

    }
    this._wrapper.innerHTML = dots
    this._dotsElems = document.querySelectorAll('.dot')
  }

  _set (num) {

    this._wrapper.querySelector('.dot_active').classList.remove('dot_active')
    this._dotsElems[num-1].classList.add('dot_active')
    this._stateIndex = num-1

  }
}
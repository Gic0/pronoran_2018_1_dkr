var d = document;


//DO NOT REMOVE TEXT BELOW (NEED FOR VISITS BUILD) >>>window.currVizitName = currVizit<<<!!!
//@currentVizitVariable@

$(() => {
  FastClick.attach(document.body);
});
window.addEventListener('touchmove', e => {
  e.preventDefault()
});

//Navigation bind
document.body.addEventListener('click', ev => {
  const href = ev.target.dataset.href;
  if (!href) return;
  //const hrefArr = href.split(",");
  //var slideIndex = hrefArr[0];
  //var lineIndex = hrefArr[1];

  nav.goTo(href);
  //nav.setLine(lineIndex);

});

//Swipe chatch

var swipeBlock = false;

var hammertime = new Hammer(document.querySelector('body'));
hammertime.get('swipe').set({ direction: Hammer.DIRECTION_ALL });

hammertime.on('swipeleft', ev => {
  if (!swipeBlock) {
    try {
      SWIPE_LEFT();
    } catch (e) {
      console.log(e);
    }
  }
});

hammertime.on('swiperight', ev => {
  if (!swipeBlock) {
    try {
      SWIPE_RIGHT();
    } catch (e) {
      console.log(e);
    }
  }
});

hammertime.on('swipeup', ev => {

  try {
    SWIPE_UP();
  } catch (e) {
    console.log(e);
  }

});

hammertime.on('swipedown', ev => {

  try {
    SWIPE_DOWN();
  } catch (e) {
    console.log(e);
  }

});


class Animator {

  constructor(el, statePreffixStr) {
    this.statePreffix = ' ' + (statePreffixStr || 'state_');

    this.slide = el;
    this.originClass = this.slide.className;
    this.stateIndex;
  }

  set(num) {
    if (num == this.stateIndex) {
      return this.stateIndex;
    }
    this.slide.className = this.originClass + this.statePreffix + num;
    this.stateIndex = num;
    return this.stateIndex;
  }

  get() {
    return this.stateIndex;
  }
}

class ExtAnimator extends Animator {
  constructor(el, statesNum) {
    super(el);
    this.stateIndex = 1;
    this.nextState = 1;
    this.statesNum = +statesNum;
  }
  next() {
    if (this.nextState == this.statesNum) {
      this.nextState = 1
      super.set(this.nextState);
    } else {
      this.nextState = +super.get() + 1;
      super.set(this.nextState);
    }
  }
}

class AnimatorLinear extends Animator {
  constructor(arg) {
    super(arg.el, arg.preffix);
    this.maxIndex = arg.maxIndex;
    this.callback = arg.callback ? arg.callback : () => { console.log('animator callback undefined') }
  }
  next() {
    let nowIndex = +this.get();

    if (nowIndex == this.maxIndex) {
      this.callback(this.get(), this.maxIndex)
      return this.maxIndex;
    } else {
      this.set(nowIndex + 1);
      this.callback(this.get(), this.maxIndex)
      return this.get();
    }

  };
  prev() {
    let nowIndex = this.get();
    if (nowIndex == 1) {
      this.callback(this.get(), this.maxIndex)
      return this.get();
    }
    this.set(this.get() - 1);
    this.callback(this.get(), this.maxIndex)
    return this.get();
  }
}

class AnimatorLoop extends Animator {
  constructor(el, preffix, max) {
    super(el, preffix);
    this.maxIndex = max;
  }
  next() {
    let nowIndex = this.get();

    if (nowIndex < this.maxIndex)
      this.set(this.get() + 1);
    else
      this.set(1);
  };
  prev() {
    this.set(this.get() - 1);
  }

}

var slides = {

  // "bp": ["bp_00_1", "bp_00_2", "bp_01", "bp_03", "bp_04", "bp_05", "bp_06"],
  "dkr": ["dkr_01", "dkr_02", "dkr_03", "dkr_04_1", "dkr_05", "bp_08"],

  default: ["dkr_01", "dkr_02", "dkr_03", "dkr_04_1", "dkr_05"]

};

class Navigation {
  constructor(objOfArrLines) {
    this.lineName = 'dkr';
    this.slides = objOfArrLines;
    this.currSlide = document.querySelector('.slide').classList[1].replace('slide_', '');
    this.lastSlide = sessionStorage.getItem('lastSlide') || null;
    this.currLine = sessionStorage.getItem('currLine') || window.currVizitName || 'default';
    console.log('initial currLine is setted - ' + this.currLine)
    this.initSlides();
  }

  dispatchInited() {
    var navInited = new CustomEvent("navInited");
    document.dispatchEvent(navInited);
  }

  initSlides() {
    try {
      this.lastLine = sessionStorage.getItem('lastLine') || 'default';
      this.currPos = this.slides[this.currLine].indexOf(this.currSlide);
      this.scenario = this.slides[this.currLine];
      this.nextSlide = this.scenario[this.currPos + 1];
      this.prevSlide = this.scenario[this.currPos - 1];
    } catch (err) {
      console.log('error Navigation class');
      console.log(err.stack);
    }
  }

  setLine(lineName) {
    this._rememberLastLine(this.currLine);
    this.currLine = lineName;
    console.log('SETTED LINE - ', lineName)
    sessionStorage.setItem('currLine', lineName);
    this.initSlides();
  }

  next() {
    if (this.nextSlide) {
      this.goTo(this.nextSlide);
    }
  }

  prev() {
    if (this.prevSlide) {
      this.goTo(this.prevSlide);
    }
  }

  goTo(slideName) {

    const slideIndex = this.slides[this.currLine].indexOf(slideName);
    if (slideIndex === 0) slideName = 'index';
    console.log(this.currLine)
    sessionStorage.setItem('currLine', this.currLine)
    document.location = slideName + '.html';
    this._rememberLastSlide(this.currSlide);
  }

  getSlideIndex() {
    this.slides[this.currLine].indexOf(slideName);
  }

  back() {
    if (this.lastSlide) {
      this.goTo(this.lastSlide);
    }
  }

  _rememberLastSlide(slideName) {
    sessionStorage.setItem('lastSlide', slideName);
  }

  _rememberLastLine(lineName) {
    sessionStorage.setItem('lastLine', lineName);
  }

}

var nav = new Navigation(slides);
nav.dispatchInited()

function SWIPE_LEFT() {
  nav.next()
};

function SWIPE_RIGHT() {
  nav.prev()
};


const hrefHandler = (href, navigatorClass) => {

  navigatorClass.goTo(href);

  return newHref;
}


var onSlideIndexChange = function(pageIndex) {
  var isiPad = navigator.vendor !== "Google Inc.";
  if (isiPad) {
    window.location = "onSlideIndexChange:" + pageIndex;
  } else {
    console.log("onSlideIndexChange:" + pageIndex);
  }
};

var pageIndex = slides.default.indexOf(nav.currSlide) + 1;
setTimeout(function() {
  onSlideIndexChange(pageIndex)
}, 300)

window.clipActionGotoSlide = function(pageIndex) {
  var index = pageIndex == 1;

  if (index) {
    window.location = 'index.html';
  } else {
    window.location = nav.slides[nav.currLine][pageIndex - 1] + '.html';
  }

};
;
(() => {
  let lib = false;
  // @if LIB
  lib = true;
  // @endif
  let $ref = $('.ref');
  let $wrapper = $('.wrapper-gray');
  if ($ref.length) {
    $('.footer__ref , .wrapper-gray').on('click', () => {
      if ($ref.hasClass('ref_show')) {
        $ref.removeClass('ref_show');
        $wrapper.fadeOut()
      } else {
        $ref.addClass('ref_show');
        $wrapper.fadeIn()
      }
    });
  } else {
    $('.footer__ref').addClass('footer__ref_disabled');
  }
  if (lib) {
    $('.footer__lib').removeClass('footer__lib_disabled').on('click', () => {
      $('.library').addClass('library_show');
    });
  }

  let ref = document.querySelector('.ref');
  let refBtn = document.querySelector('.footer__ref');
  let wrapper = document.querySelector('.wrapper-gray');
  let footer = document.querySelector('.footer');
})();
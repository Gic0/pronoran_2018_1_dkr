var myElement = document.querySelector('.logo');

// if(myElement){
//   var mc = new Hammer.Manager(myElement);
//   mc.add( new Hammer.Tap({ event: 'doubletap', taps: 2 }) );
//   mc.on("doubletap", function(event) {
//     if ($('.help').hasClass('help_active')) $('.help_active').addClass('help_show');
//     event.preventDefault();
//     console.log('logo clicked');
//   });
// }

var tapped=false;
$(".logo").on("touchstart",function(e){
  if(!tapped){ //if tap is not set, set up single tap
    tapped=setTimeout(function(){
        tapped=null;
        //insert things you want to do when single tapped
    },300);   //wait 300ms then run single click code
  } else {    //tapped within 300ms of last tap. double tap
    clearTimeout(tapped); //stop single tap callback
    tapped=null;
    $('.help_active').addClass('help_show');
    console.log("logo double clicked!");
  }
  e.preventDefault();
});

$('.help__header, .help__body-arrow').on('click', ()=>{
  $('.help__header-plus').toggleClass('help__header-plus_active');
  $('.help__body').toggleClass('help__body_show');
});

$('.help__overlay').on('click', ()=>{
  $('.help').removeClass('help_show');
});

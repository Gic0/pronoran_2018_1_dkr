;
(() => {
  $('.popup__close, .overlay').on('click', (e) => {
    $('.overlay').removeClass('overlay_show');
    $('.popup').removeClass('popup_show');
  });
  $('.popup-btn').on('click', () => {
    console.log(this)
    $('.overlay').addClass('overlay_show');
    $('.popup').addClass('popup_show');
  });
})();
#!/bin/bash

JADETMP="extends ../../blocks/layout/layout
block content
  .slide_wrapper


  +footer()

  .ref    
    | ref

  +help('Цель анимации:').help_active
    +hint('pack')
    +hint('ref')
    +hint('right', 'Нажмите, чтобы перейти в соответсвующий сценарий')
"
JSTMP=" "
CSSTMP=".slide_$1 {
  
}
"

mkdir app/slides/$1 &&
touch app/slides/$1/$1.js && 
touch app/slides/$1/$1.css &&
touch app/slides/$1/$1.jade
mkdir app/slides/$1/img -p &&
echo "$JSTMP" >>  ./app/slides/$1/$1.js &&
echo "$CSSTMP" >>  ./app/slides/$1/$1.css &&
echo "$JADETMP" >>  ./app/slides/$1/$1.jade

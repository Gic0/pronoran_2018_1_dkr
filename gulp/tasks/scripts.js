import gulp         from 'gulp';
import plumber      from 'gulp-plumber';
import concat       from 'gulp-concat';
import babel        from 'gulp-babel';
import rename       from 'gulp-rename';
import changed      from 'gulp-changed';
import gutil        from 'gulp-util';
import errorHandler from '../utils/errorHandler';
import paths        from '../paths';

gulp.task('scripts:blocks', () => {
  gulp.src('app/blocks/**/*.js')
    .pipe(plumber({errorHandler: errorHandler}))
    .pipe(babel())
    .pipe(concat('main.js'))
    .pipe(changed(paths.scripts))
    .pipe(gulp.dest(paths.scripts))
});

gulp.task('scripts:slides', () => {
  gulp.src('app/slides/**/*.js')
    .pipe(plumber({errorHandler: errorHandler}))
    .pipe(rename((path)=>{
      path.basename = 'slide';
    }))
    .pipe(changed(paths.scripts))
    .pipe(babel({
        blacklist: ["useStrict"],
    },))
    .pipe(gulp.dest(paths.scripts))
});

gulp.task('scripts:libs', () => {
  gulp.src('app/scripts/**/*.js')
    .pipe(plumber({errorHandler: errorHandler}))
    .pipe(changed(paths.scripts))
    .pipe(gulp.dest(paths.scripts))
});

gulp.task('scripts', ['scripts:blocks','scripts:slides','scripts:libs'])

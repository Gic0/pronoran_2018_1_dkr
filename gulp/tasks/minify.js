import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import pngquant from 'imagemin-pngquant';

gulp.task('minify:image', function(){
  return gulp.src('dist/**/*.{png,jpg,gif}')
    .pipe(imagemin({
      optimizationLevel: 5,
      progressive: true,
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest('dist/'))
});

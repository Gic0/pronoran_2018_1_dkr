import runSequence from 'run-sequence';
import gulp from 'gulp';
import gutil from 'gulp-util';

gulp.task('default', () => (
  runSequence('copy', [
      'scripts',
      'style',
      'jade'
    ],
    'server',
    'watch'
  )
));
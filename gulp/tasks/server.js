import browserSync from 'browser-sync';
import gulp        from 'gulp';
import gutil       from 'gulp-util';

gulp.task('server', () => (
	browserSync.init({
		files: ['dist/**/*'],
		open: !!gutil.env.open,
		reloadOnRestart: true,
		port: gutil.env.port || 3000,
		server: {
			baseDir: [
				'dist',
				'zip'
			],
			directory: true,
		},
		tunnel: !!gutil.env.tunnel
	})
));

gulp.task('server_b', () => (
	browserSync.init({
		files: ['build/**/*'],
		open: !!gutil.env.open,
		reloadOnRestart: true,
		port: gutil.env.port || 3000,
		server: {
			baseDir: [
				'build'
			],
			directory: true,
		},
		tunnel: !!gutil.env.tunnel
	})
));

gulp.task('server:v', () => (
	browserSync.init({
		files: ['vizits/**/*'],
		open: !!gutil.env.open,
		reloadOnRestart: true,
		port: gutil.env.port || 3000,
		server: {
			baseDir: [
				'vizits'
			],
			directory: true,
		},
		tunnel: !!gutil.env.tunnel
	})
));

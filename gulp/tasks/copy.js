import gulp    from 'gulp';
import changed from 'gulp-changed';
import filter  from 'gulp-filter';
import gutil   from 'gulp-util';
import gulpif  from 'gulp-if';
import rename from 'gulp-rename';
import paths   from '../paths';

gulp.task('copy:images-blocks', () => (
	gulp.src([
			'app/blocks/**/*.{png,jpg,gif,pdf,mp4,svg}'
		])
		.pipe(rename(function (path) {
			path.dirname = '/blocks/' + path.dirname.replace('img', '')
		}))
		.pipe(changed(paths.img))
		.pipe(gulp.dest(paths.img))
));


gulp.task('copy:video-blocks', () => (
	gulp.src([
			'app/blocks/**/*.{mp4,mov,webm,mkv,avi}'
		])
		.pipe(rename(function (path) {
			path.dirname = '/blocks/' + path.dirname.replace('img', '')
		}))
		.pipe(changed(paths.img))
		.pipe(gulp.dest(paths.img))
));




gulp.task('copy:preview', () => (
	gulp.src([
			'app/*.{png,jpg}'
		])
		.pipe(changed(paths.dist))
		.pipe(gulp.dest(paths.dist))
));

gulp.task('copy:images-slides', () => (
	gulp.src([
			'app/slides/**/*.{png,jpg,gif,mp4,pdf,svg}'
		])
		.pipe(rename(function (path) {
			path.dirname = '/slides/' + path.dirname.replace('img', '')
		}))
		.pipe(changed(paths.img))
		.pipe(gulp.dest(paths.img))
));
gulp.task('copy:video-slides', () => (
	gulp.src([
			'app/slides/**/*.{mp4,mov,webm,mkv,avi}'
		])
		.pipe(rename(function (path) {
			path.dirname = '/slides/' + path.dirname.replace('img', '')
		}))
		.pipe(changed(paths.img))
		.pipe(gulp.dest(paths.img))
));

gulp.task('copy:fonts', () => (
	gulp.src([
			'app/fonts/*.*'
		])
		.pipe(changed(paths.fonts))
		.pipe(gulp.dest(paths.fonts))
));


gulp.task('copy:pdf' , ()=>{
	gulp.src([
			'app/pdf/*.*'
		])
		.pipe(gulp.dest('dist/pdf'))
});

gulp.task('copy', [
	'copy:images-blocks',
	'copy:images-slides',
	'copy:video-blocks',
	'copy:video-slides',
	'copy:preview',
	'copy:pdf',
	'copy:fonts'
]);

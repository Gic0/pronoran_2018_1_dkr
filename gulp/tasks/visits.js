import gulp from 'gulp';
import del from 'del';
import rename from 'gulp-rename';
import { vizits } from '../../app/vizits';
import replace from 'gulp-replace';


function testForMainJs(file) {
  return true
}

gulp.task('vizits', () => {

  console.log('removing previous VIZITS...')
  del.sync('./vizits');

  const vizitNamesArr = Object.keys(vizits);

  vizitNamesArr.forEach((vizitName, vizitIndex) => {

    const currVizit = vizitName;
    const slidesArr = vizits[vizitName];

    const pdfPath = `dist/pdf/**/*`;
    const currVizitGlobalJs = `dist/js/*.js`;
    const currVizitGlobaCss = `dist/css/*.css`;
    const currVizitGlobalImg = `dist/img/blocks/**/*`;

    gulp.src(pdfPath).pipe(gulp.dest('vizits/' + currVizit + '/pdf'));

    gulp.src(currVizitGlobalJs)
      .pipe(replace('//@currentVizitVariable@', `window.currVizitName = '${vizitName}';`))
      .pipe(gulp.dest('vizits/' + currVizit + '/js'))

    gulp.src(currVizitGlobaCss).pipe(gulp.dest('vizits/' + currVizit + '/css'));
    gulp.src(currVizitGlobalImg).pipe(gulp.dest('vizits/' + currVizit + '/img/blocks'));

    slidesArr.forEach((slide, i) => {

      const currSlideHtml = `dist/${slide}.html`;
      const currSlideJsPath = `dist/js/${slide}/**/*`;
      const currSlideCssPath = `dist/css/${slide}/**/*`;
      const currSlideImgPath = `dist/img/slides/${slide}/**/*`;
      const currSlideThumbImgPath = `dist/thumbs/${slide}.jpg`;

      gulp.src(currSlideHtml).pipe(rename((path) => { if (i == 0) path.basename = 'index'; }))
        .pipe(gulp.dest('vizits/' + currVizit + '/'));

      gulp.src(currSlideJsPath).pipe(gulp.dest('vizits/' + currVizit + '/js/' + slide));
      gulp.src(currSlideCssPath).pipe(gulp.dest('vizits/' + currVizit + '/css/' + slide));
      gulp.src(currSlideImgPath).pipe(gulp.dest('vizits/' + currVizit + '/img/slides/' + slide));
      gulp.src(currSlideThumbImgPath).pipe(rename((path) => { path.basename = i + 1 }))
        .pipe(gulp.dest('vizits/' + currVizit + '/thumbs/'));

    })

    console.log(vizitName + ' built!')

  });

});
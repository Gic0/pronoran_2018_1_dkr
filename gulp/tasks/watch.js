import gulp from 'gulp';
import runSequence from 'run-sequence';
import { reload } from 'browser-sync';

gulp.task('watch', () => {
  global.watch = true;

  gulp.watch(['app/styles/**/*.css', 'app/blocks/**/*.css'], ['style:blocks']);

  gulp.watch(['app/slides/**/*.css'], ['style:slides']);

  gulp.watch('app/{slides,blocks}/**/*.jade', () => runSequence('jade'));

  gulp.watch('app/{slides,blocks}/**/*.js', () => runSequence('scripts'));

  gulp.watch('app/{slides,blocks}/**/*.{png,jpg,gif,mp4,mov,webm,svg}', ['copy', reload]);

});
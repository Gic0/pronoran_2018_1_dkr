import gulp from 'gulp';
import plumber from 'gulp-plumber';
import changed from 'gulp-changed';
import concat from 'gulp-concat';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import assets from 'postcss-assets';
import nested from 'postcss-nested';
import autoreset from 'postcss-autoreset';
import forLoop from 'postcss-for';
import calc from 'postcss-calc';
import browserSync from 'browser-sync';
import glob from 'glob';
import errorHandler from '../utils/errorHandler';
import paths from '../paths';

gulp.task('style:blocks', function() {
  let processors = [
    forLoop,
    calc,
    nested,
    assets({
      loadPaths: ['img/blocks/'],
      basePath: 'dist/',
      baseUrl: '../',
    }),
    autoprefixer({
      browsers: ['last 40 version'],
    }),
  ];
  return gulp.src(['app/styles/**/*.css', 'app/blocks/**/*.css'])
    .pipe(plumber({
      errorHandler: errorHandler,
    }))
    .pipe(concat('main.css'))
    .pipe(changed('dist/css/'))
    .pipe(postcss(processors))
    .pipe(gulp.dest('dist/css/'));
});

gulp.task('style:slides', function() {
  glob.sync('*', {
    cwd: 'app/slides/',
  }).map((slide) => {
    let processors = [
      forLoop,
      calc,
      nested,
      assets({
        loadPaths: ['./img/slides/' + slide],
        basePath: 'dist/',
        baseUrl: '../../',
      }),
      autoprefixer({
        browsers: ['last 40 version'],
      }),
    ];
    return gulp.src(['app/slides/' + slide + '/*.css'])
      .pipe(plumber({
        errorHandler: errorHandler,
      }))
      .pipe(concat('slide.css'))
      .pipe(changed('dist/css/' + slide))
      .pipe(postcss(processors))
      .pipe(gulp.dest('dist/css/' + slide));
  });
});

gulp.task('style', [
  'style:blocks',
  'style:slides',
]);

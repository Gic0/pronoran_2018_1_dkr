import gulp from 'gulp';
import Pageres from 'pageres';
import glob from 'glob';
import path from 'path';


let sortIndexFirst = (arr) => {

  let indexHtml = arr.indexOf('dist/index.html');

  if (indexHtml > 0) {
    let tmp;
    tmp = arr[indexHtml];
    arr[indexHtml] = arr[0];
    arr[0] = tmp;
  }

  return arr;

}


gulp.task('screens', () => {
  let pageres = new Pageres();
  sortIndexFirst(glob.sync('dist/*.html')).map((file) => {
    let name = path.basename(file, '.html');
    pageres.src(file, ['1024x768'], { crop: true, filename: name, scale: 1, delay: 5, format: 'jpg' });
  });
  pageres.dest('dist/thumbs/').run().then(() => { console.log('done') });
});
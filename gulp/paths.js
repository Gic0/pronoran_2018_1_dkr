export default {
	dist: 'dist',
	img: 'dist/img',
	fonts: 'dist/fonts',
	scripts: 'dist/js',
  project: 'erespal_terapevts_2016_2'
};

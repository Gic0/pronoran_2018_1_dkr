﻿# Сборка #

### Визиты более не используются ###

### 1) vizits.json (app/vizits.json) ###

структура визитов (vizits), необходимая для сборки визитов, и имена для zip архивов (vizitNames)

```js

const vizits = {
  "bp": ["bp_02", "bp_04", "bp_05", "bp_06", "bp_07"],
  "ukr": ["ukr_02", "ukr_03", "ukr_04", "ukr_05_1", "ukr_06"]
};

const vizitNames = {
  "bp": 'Болезнь_Паркинсона',
  "ukr": 'УКР'
};

module.exports = {
  vizits,
  vizitNames
}

```

### 2) layout.js (app/blocks/layout/layout.js) ###

дублирующая структура визитов, необходимая для навигации по сценариям, по умолчанию будет default (для разработки)

```js

var slides = {

  "bp": ["bp_02", "bp_04", "bp_05", "bp_06", "bp_07"],
  "ukr": ["ukr_02", "ukr_03", "ukr_04", "ukr_05_1", "ukr_06"],

  default: ["bp_02", "bp_04", "bp_05", "bp_06", "bp_07", "ukr_02", "ukr_03", "ukr_04", "ukr_05_1", "ukr_06"]

};

```

### 3) GULP TASKS ###

| name | info |
| ------ | ------ |
| `gulp default` | собирает слайды и запускает сервер, в ./dist |
| `gulp screens` | запустить для создания скринов, Скрины попадают в ./dist/thumbs |
| `gulp vizits` | собирает визиты со всеми нужными файлами и скринами в ./vizits |
| `gulp zip` | собирает визиты в zip архивы в ./zip, имена архивов берет из app/vizits.json |
